<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 20/08/2015
 * Time: 19:03
 */

namespace CodeProject\OAuth;

use Illuminate\Support\Facades\Auth;

class Verifier {
    public function verify($username, $password)
    {
        $credentials = [
            'email'    => $username,
            'password' => $password,
        ];

        if (Auth::once($credentials)) {
            return Auth::user()->id;
        }

        return false;
    }
}