<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 04/08/2015
 * Time: 20:58
 */

namespace CodeProject\Repositories;


use CodeProject\Entities\User;
use Prettus\Repository\Eloquent\BaseRepository;

class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    public function Model(){
        return User::class;
    }
}