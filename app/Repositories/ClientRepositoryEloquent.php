<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 04/08/2015
 * Time: 20:58
 */

namespace CodeProject\Repositories;


use CodeProject\Entities\Client;
use Prettus\Repository\Eloquent\BaseRepository;

class ClientRepositoryEloquent extends BaseRepository implements ClientRepository
{
    public function Model(){
        return Client::class;
    }
}