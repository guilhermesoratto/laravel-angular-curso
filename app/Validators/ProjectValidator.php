<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 05/08/2015
 * Time: 00:10
 */

namespace CodeProject\Validators;


use Prettus\Validator\LaravelValidator;

class ProjectValidator extends LaravelValidator{

    protected $rules = [
        'owner_id' => 'required|integer',
        'client_id' => 'required|integer',
        'name' => 'required',
        'progress' => 'required',
        'status' => 'required',
        'due_date' => 'required',
    ];
}