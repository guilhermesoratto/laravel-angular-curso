<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 05/08/2015
 * Time: 00:10
 */

namespace CodeProject\Validators;


use Prettus\Validator\LaravelValidator;

class ProjectNoteValidator extends LaravelValidator{

    protected $rules = [
        'project_id' => 'required|integer',
        'title' => 'required',
        'note' => 'required'
    ];
}