<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 11/09/2015
 * Time: 15:15
 */

namespace CodeProject\Transformers;

use CodeProject\Entities\User;
use League\Fractal\TransformerAbstract;

class ProjectOwnerTransformer extends TransformerAbstract{


    public function transform(User $owner){
        return [
            'name' =>$owner->name,
            'email' => $owner->email
        ];
    }
}