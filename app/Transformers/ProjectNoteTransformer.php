<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 11/09/2015
 * Time: 15:15
 */

namespace CodeProject\Transformers;

use CodeProject\Entities\ProjectNote;
use League\Fractal\TransformerAbstract;

class ProjectNoteTransformer extends TransformerAbstract{


    public function transform(ProjectNote $projectNote){
        return [
            'id' => $projectNote->id,
            'title' => $projectNote->title,
            'note' => $projectNote->note,
            'project_id' => $projectNote->project->id
        ];
    }


}