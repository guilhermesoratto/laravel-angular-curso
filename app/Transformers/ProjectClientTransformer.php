<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 11/09/2015
 * Time: 15:15
 */

namespace CodeProject\Transformers;

use CodeProject\Entities\Client;
use League\Fractal\TransformerAbstract;

class ProjectClientTransformer extends TransformerAbstract{


    public function transform(Client $client){
        return [
            'client_id' => $client->id,
            'name' =>$client->name,
            'responsible' => $client->responsible,
            'email' => $client->email,
            'phone' => $client->phone,
            'address' => $client->address,
            'obs' => $client->obs
        ];
    }
}