<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 11/09/2015
 * Time: 15:15
 */

namespace CodeProject\Transformers;

use CodeProject\Entities\Project;
use League\Fractal\TransformerAbstract;

class ProjectTransformer extends TransformerAbstract{

    protected $defaultIncludes = ['members', 'clients', 'owner'];

    public function transform(Project $project){
        return [
            'project_id' => $project->id,
            'name' => $project->name,
            'description' => $project->description,
            'progress' => $project->progress,
            'status' => $project->status,
            'due_date' => $project->due_date
        ];
    }

    public function includeMembers(Project $project)
    {
        return $this->collection($project->members, new ProjectMemberTransformer());
    }

    public function includeClients(Project $project){
        return $this->collection($project->clients, new ProjectClientTransformer());
    }

    public function includeOwner(Project $project){
        return $this->collection($project->owner, new ProjectOwnerTransformer());
    }
}