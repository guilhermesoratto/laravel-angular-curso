<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 11/09/2015
 * Time: 15:28
 */

namespace CodeProject\Presenters;

use CodeProject\Transformers\ProjectTransformer;
use Prettus\Repository\Presenter\FractalPresenter;


class ProjectPresenter extends FractalPresenter {

    public function getTransformer()
    {
        return new ProjectTransformer();
    }
}