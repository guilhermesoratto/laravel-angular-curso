<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Resetar dados da tabela;
        //\CodeProject\Entities\Project::truncate();
        //Criar 10 registros aleatórios na tabela client
        factory(\CodeProject\Entities\User::class)->create([
            'name' => 'Guilherme',
            'email' => 'guilherme.soratto@gmail.com',
            'password' => bcrypt('123456'),
            'remember_token' => str_random(10),
        ]);
        factory(\CodeProject\Entities\User::class, 10)->create();


    }
}
