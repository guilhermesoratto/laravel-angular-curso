<?php

use Illuminate\Database\Seeder;

class ProjectNoteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Resetar dados da tabela;
        //\CodeProject\Entities\Project::truncate();
        //Criar 10 registros aleatórios na tabela client
        factory(\CodeProject\Entities\ProjectNote::class, 50)->create();


    }
}
