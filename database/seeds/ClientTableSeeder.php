<?php

use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Resetar dados da tabela;
        //\CodeProject\Entities\Client::truncate();
        //Criar 10 registros aleatórios na tabela client
        factory(\CodeProject\Entities\Client::class, 10)->create();


    }
}
