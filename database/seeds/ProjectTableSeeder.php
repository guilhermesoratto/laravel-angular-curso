<?php

use Illuminate\Database\Seeder;

class ProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Resetar dados da tabela;
        //\CodeProject\Entities\Project::truncate();
        //Criar 10 registros aleatórios na tabela client
        factory(\CodeProject\Entities\Project::class, 10)->create();


    }
}
